package cn.youth.datacollection.service;

import cn.youth.datacollection.entity.KFailRetryLog;
import com.baomidou.mybatisplus.extension.service.IService;

public interface KFailRetryLogService extends IService<KFailRetryLog> {
}
