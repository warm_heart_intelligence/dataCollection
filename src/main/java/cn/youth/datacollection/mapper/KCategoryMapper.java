package cn.youth.datacollection.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.youth.datacollection.entity.KCategory;

public interface KCategoryMapper extends BaseMapper<KCategory> {

}
