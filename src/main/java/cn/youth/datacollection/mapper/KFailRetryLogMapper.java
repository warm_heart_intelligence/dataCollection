package cn.youth.datacollection.mapper;

import cn.youth.datacollection.entity.KFailRetryLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface KFailRetryLogMapper extends BaseMapper<KFailRetryLog> {
}
